<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableKaryaIlmiah extends Migration
{

    public function up()
    {
         Schema::create('karya_ilmiah', function (Blueprint $table) {
          $table->increments('id');
          $table->string('judul');
          $table->text('isi');
          $table->string('calon_id');
          $table->timestamps();
        });
    }

    public function down()
    {
    Schema::dropIfExists('karya_ilmiah');
    }
}
