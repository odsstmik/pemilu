@extends('manager.layout.index')
@section('content')
<div class="page-header">
  <h3>User</h3>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-default btn-sm"><a href="{{url('/user/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
  </div>
  <div class="panel-body">

  <table class="table table-default">
    <thead>
      <tr>
        <th>id</th>
        <th>Nama</th>
        <th>Email</th>
        <th>Password</th>
        <th>Status</th>
        <th>Role</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($users as $index=>$item)
      <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->name}}</td>
        <td>{{$item->email}}</td>
        <td>{{$item->password}}</td>
        <td>{{$item->status_label}}</td>
        <td>{{$item->role_label}}</td>
        <td>
          <a href="{{url('user/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
