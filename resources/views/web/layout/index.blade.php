<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PEMILU STIKOM</title>
<link rel="stylesheet" href="{{url('template/front/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('template/front/css/user.css')}}">
<link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}">
</head>
	<div id="menu">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="{{url('/')}}">PEMILU BEM STMIK</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">

          <li><a href="{{url('/panitia')}}">Panitia</a></li>
				<li><a href="{{url('/kandidat')}}"> Kandidat</a></li>
			  </ul>

			  <ul class="nav navbar-nav navbar-right">
				
				<li><a href="{{url('/login')}}" data-toggle="tooltip" data-placement="bottom" title="Log in"><i class="fa fa-power-off" style="color:blue;"></i></a></li>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div id="wrapper">
			@yield('list_nav')
			<div class="container">
				@yield('content')
			</div>
		</div>
	</div>
	<!--script-->
	<script src="{{url('template/front/js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('template/front/js/bootstrap.min.js')}}"></script>
<body>
</body>
</html>
