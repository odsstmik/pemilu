@extends('web.layout.index')
@section('list_nav')
  <ol class="breadcrumb" style="margin-top:-16px;">
    <li><a href="{{url('/')}}">Halaman Utama</a></li>
    <li class="active">panitia</li>
  </ol>
@endsection
@section('content')
<div class="row">
  @foreach($panitia as $panitia)
  <div class="col-sm-4">
    <div class="panel panel-default">
        <div class="panel-body">
          <div class="row">
            <div class="col-sm-4">
              <img src="{{url('template/front/img/logo_bg.png')}}" alt="" class="img-responsive">
            </div>
            <div class="col-sm-8">
              <b> {{$panitia->nama}}</b>
              <p>{{$panitia->jabatan}}</p>
            </div>
          </div>
        </div>
    </div>
  </div>
  @endforeach
</div>
@endsection
