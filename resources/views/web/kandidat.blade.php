@extends('web.layout.index')
@section('list_nav')
  <ol class="breadcrumb" style="margin-top:-16px;">
    <li><a href="{{url('/')}}">Halaman Utama</a></li>
    <li class="active">Kandidat</li>
  </ol>
@endsection
@section('content')
<div id="kandidat_" style="min-height:100vh;">
    <div class="row">
        @foreach($kandidat as $index=>$item)
        <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="panel panel-default" style="background:transparent;border:0px;">
            <div class="panel-body">
              <div class="row">
                <div class="col-sm-4 col-md-4 col-lg-4">
                    <img src="{{$item->image}}" alt="" class="img-responsive" width="300">
                </div>
                <div class="col-sm-8 col-md-8 col-lg-8">
                  <h2 class="te"> PASLON {{$item->nomor}} </h2>
                  <div class="text_">
                    <h5 class="te"> Ketua :  {{$item->ketua}} </h5>
                    <h5 class="te"> Wakil :  {{$item->wakil}} </h5>
                    <br>
                    <p>visi di taro sini </p>
                    <p>misi di taro sinis</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div >

          </div>



        </div>
          @endforeach
    </div>
</div>

@endsection
