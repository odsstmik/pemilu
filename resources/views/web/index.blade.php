<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
	<title>Pemilihan Umum</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">

	<link rel="stylesheet" href="{{ url('template/front/css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{ url('template/front/css/index.css')}}">
	<link rel="stylesheet" href="{{ url('template/front/css/animate.css')}}">

		<!-- Important Owl stylesheet -->
	<link rel="stylesheet" href="{{ url('template/front/css/owl.carousel.css')}}">

	<!-- Default Theme -->
	<link rel="stylesheet" href="{{ url('template/front/css/owl.theme.css')}}">

</head>


		<div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
		  <div class="modal-dialog modal-lg" role="document">
			<div class="modal-content">
			 <div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title">PEMILIHAN UMUM STMIK</h4>
			  </div>
			  <div class="modal-body">
			  	<ul class="menu_modal">
			  		<li><a href="{{url('/kandidat')}}" class="btn">Kandidat</a></li>
			  		<li><a href="{{url('/panitia')}}" class="btn">Panitia</a></li>
			  		<li><a href="#" class="btn">Hasil Voting</a></li>
			  		<li><a href="{{url('/about')}}" class="btn">Tentang Kami</a></li>
			  	</ul>
			  </div>
			</div>
		  </div>
		</div>
	<div id="wrap-all">
<!--	modal -->
	<button id="modal_" type="button" class="btn " data-toggle="modal" data-target=".bs-example-modal-lg"><i class="glyphicon glyphicon-th"></i></button>


		<div id="menu">
			<nav class="navbar navbar-default">
			  <div class="container">
			  	<ul class="nav navbar-nav navbar-right">
			  		<li><a href="{{url('/kandidat')}}">Kandidat</a></li>
			  		<li><a href="{{url('/panitia')}}">Panitia</a></li>
			  		<li><a href="#">Hasil Voting</a></li>
			  		<li><a href="{{url('/about')}}">Tentang Kami</a></li>
			  	</ul>
			  </div>
			</nav>
		</div>

		<div class="img_" style="z-index: 9999;">

			<img src="{{ url('template/front/img/logo_bg.png')}}" class="img-responsive center-block animated fadeIn" width="80%">
		</div>
		<div class="container" style="z-index: 99;">
			<div class="row">
				<div id="people">
					<div class="col-sm-6 col-md-6 col-lg-6">
					<br>
					<br>
						<div id="owl-example" class="owl-carousel owl-theme ">
							@foreach($kandidat as $index=>$item)
							<div class="owl-item animated fadeIn">
									<img src="{{$item->image}}" class="img-responsive img-circle img_people owl-fade-in" width="250px" height="250px">
									<br>
									<h1>NOMOR URUT {{$item->nomor}}</h1>
									<h3 style="margin-top:-10px;">{{$item->ketua}} <br>& {{$item->wakil}}</h3>
									<p> <b>" {{$item->visi}}"</b></p>
									<br>
								<button class="btn btn-primary"><a href="#">Lihat selengkapnya</a></button>
							</div>
							@endforeach
						</div>
					</div>
				</div>
				<div id="text_">
				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="text_right animated bounceInRight">
						<br>
						<br>
						<br>
						<br>
						<h1>LOGIN <br> </h1>
						<h2 style="margin-top: -10px;">UNTUK MEMILIH</h2>
						<a href="{{url('/login')}}"><button class="btn btn-primary"> L o g i n </button></a>
					</div>
				</div>

				<div class="col-sm-6 col-md-6 col-lg-6">
					<div class="text_right_2 animated fadeIn text-right">
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<br>
						<h5 style="margin-top: -10px;">Presented by</h5>
						<ul>
							<li style="list-style: none;"><img src="" alt="">BEM STMIK BALIKPAPAN  </li>
							<li style="list-style: none;"><img src="" alt="">ODS STMIK BALIKPAPAN </li>
						</ul>

					</div>
				</div>
				</div>

				<div id="text_1">
					<button class="btn btn-success btn-sm"><a href="{{url('/login')}}">Login</a></button>
					<br>
					<br>
					<h1>PEMILIHAN </h1>
					<h3 style="float: right;margin-top: -5px;">UMUM</h3 >
				</div>
			</div>
		</div>
	</div>
	<!--script-->
	<script src="{{url('template/front/js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ url('template/front/js/bootstrap.min.js')}}"></script>
        <!-- Include js plugin -->
    <script src="{{ url('template/front/js/owl.carousel.js')}}"></script>

    <script>

		$('#wrap-all').mousemove(function( event ) {

		var containerWidth = $(this).innerWidth(),
			containerHeight = $(this).innerHeight(),
			mousePositionX = (event.pageX / containerWidth) * 100,
			mousePositionY = (event.pageY /containerHeight) * 100;

		$(this).css('background-position', mousePositionX + '%' + ' ' + mousePositionY + '%');

		});

		$(document).ready(function() {

 		var owl = $("#owl-example");

		  owl.owlCarousel({
		  	baseClass : "owl-carousel",
			baseClass : "animated",
   			theme : "owl-theme",
			navigation: true,
   			autoPlay: 5000,
			  slideSpeed:300,
   			 singleItem:true,

			  // "singleItem:true" is a shortcut for:
			  items : 1,
			  itemsDesktop : false,
			  itemsDesktopSmall : false,
			  itemsTablet: false,
			  itemsMobile : false,
			  addClassActive:true,
			  transitionStyle: "fade",


		  });

		});
	</script>

<body>
</body>
</html>
