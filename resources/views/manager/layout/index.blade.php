@if(Auth::User()->status=='0')
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Bootstrap Admin Template</title>

    <link href="{{url('template/admin/vendor/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

    <link href="{{url('template/admin/vendor/metisMenu/metisMenu.min.css')}}" rel="stylesheet">

    <link href="{{url('template/admin/dist/css/sb-admin-2.css')}}" rel="stylesheet">

    <link href="{{url('template/admin/vendor/morrisjs/morris.css')}}" rel="stylesheet">

    <link href="{{url('template/admin/vendor/font-awesome/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">

</head>

<body>

    <div id="wrapper">
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="{{url('/home')}}">Pemilu STMIK Balikpapan</a>
            </div>

            @include('manager.layout.nav-top')
            <!-- /.navbar-top-links -->

            @include('manager.layout.menu-sidebar')
            <!-- /.navbar-static-side -->
        </nav>

        <div id="page-wrapper">
       		<br>
            <!-- /.row -->
            @yield('content')
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="{{url('template/admin/vendor/jquery/jquery.min.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('template/admin/vendor/bootstrap/js/bootstrap.min.js')}}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{url('template/admin/vendor/metisMenu/metisMenu.min.js')}}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{url('template/admin/vendor/raphael/raphael.min.js')}}"></script>
    <script src="{{url('template/admin/vendor/morrisjs/morris.min.js')}}"></script>
    <script src="{{url('template/admin/data/morris-data.js')}}"></script>

    <!-- Custom Theme JavaScript -->
    <script src="{{url('template/admin/dist/js/sb-admin-2.js')}}"></script>

</body>

</html>
@else
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>PEMILU STIKOM</title>
<link rel="stylesheet" href="{{url('template/front/css/bootstrap.min.css')}}">
<link rel="stylesheet" href="{{url('template/front/css/user.css')}}">
<link rel="stylesheet" href="{{url('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css')}}">
<style media="screen">
  a{
    text-decoration: none !important;
  }
</style>
</head>
	<div id="menu">
		<nav class="navbar navbar-default">
		  <div class="container-fluid">
			<!-- Brand and toggle get grouped for better mobile display -->
			<div class="navbar-header">
			  <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="navbar-brand" href="{{url('/home')}}">BEM STMIK BALIKPAPAN</a>
			</div>

			<!-- Collect the nav links, forms, and other content for toggling -->
			<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
			  <ul class="nav navbar-nav">

				<li><a href="{{url('/home')}}">Dashboard</a></li>
        <li><a href="{{url('/home/panitia')}}">Panitia</a></li>
        <li><a href="#">Hasil Vote</a></li>
        <li><a href="{{url('/coment/create')}}">Komentar</a></li>
			  </ul>
			  <ul class="nav navbar-nav navbar-right">

				<li><a href="#"><i class="fa fa-envelope"></i></a></li>
				<li><a href="{{url('logout')}}" onclick="event.preventDefault();
                 document.getElementById('logout-form').submit();"><i class="fa fa-power-off" style="color:red;"></i> Log Out</a></li>
                 <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                     {{ csrf_field() }}
               </form>
			  </ul>
			</div><!-- /.navbar-collapse -->
		  </div><!-- /.container-fluid -->
		</nav>
		<div id="wrapper">
			<div class="container-fluid">
				@yield('content')
			</div>
		</div>
	</div>
	<!--script-->
	<script src="{{url('template/front/js/jquery.js')}}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{url('template/front/js/bootstrap.min.js')}}"></script>
<body>
</body>
</html>

@endif
