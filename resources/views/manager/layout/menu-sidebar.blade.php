<div class="navbar-default sidebar" role="navigation">
    <div class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
           <li class="sidebar-search text-center">
               <img src="{{url('template/admin/img/profil.jpg')}}" class="img-responsive img_profil center-block" width="80">
                <br>
                <p style="color: white;">  {{ Auth::user()->name }}</p>
                <br>
                <!-- /input-group -->
            </li>
            <li>
                <a href="{{url('/home')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>
            <li>
                <a href="{{url('/home/users')}}"><i class="fa fa-dashboard fa-fw"></i> User</a>
            </li>
            <li>
                <a href="{{url('/home/kandidat')}}"><i class="fa fa-table fa-fw"></i> Kandidat</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Visi & Misi<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('/home/visi')}}">Visi</a>
                    </li>
                    <li>
                        <a href="{{url('/home/misi')}}">Misi</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
            <li>
                <a href="{{url('/home/panitia')}}"><i class="fa fa-table fa-fw"></i> Panitia</a>
            </li>
            <li>
                <a href="{{url('/home/karya')}}"><i class="fa fa-edit fa-fw"></i> Karya Ilmiyah</a>
            </li>
            <li>
                <a href="{{url('/home/coment')}}"><i class="fa fa-edit fa-fw"></i> Komentar</a>
            </li>
            <li>
                <a href="#"><i class="fa fa-wrench fa-fw"></i> Vote<span class="fa arrow"></span></a>
                <ul class="nav nav-second-level">
                    <li>
                        <a href="{{url('home/voting')}}">Voting</a>
                    </li>
                    <li>
                        <a href="{{url('home/vote')}}">Voter</a>
                    </li>
                    <li>
                        <a href="{{url('vote/read')}}">Vote</a>
                    </li>
                </ul>
                <!-- /.nav-second-level -->
            </li>
        </ul>
    </div>
    <!-- /.sidebar-collapse -->
</div>
