@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/misi')}}">Misi</a></li>
  <li class="active">Form Misi</li>
</ol>
  <div class="page-header">
    <h3>Input Data Misi</h3>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Misi
        </div>
          <div class="panel-body">
            {!! Form::open() !!}
            {!! Form::model($misi) !!}


            <div class="form-group{{ $errors->has('misi') ? ' has-error' : '' }}">
              {!! Form::label('misi') !!}
              {!! Form::textarea('misi', old('misi'),['class'=>'form-control']) !!}
              @if ($errors->has('misi'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('misi') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('calon_id') ? ' has-error' : '' }}">
              {!! Form::label('calon ') !!}
              {!! Form::select('calon_id', $kandidat, old('calon_id'),['class'=>'form-control']) !!}
              @if ($errors->has('calon_id'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('calon_id') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>

@endsection
