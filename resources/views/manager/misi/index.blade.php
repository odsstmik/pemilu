@extends('manager.layout.index')
@section('content')
<div class="page-header">
  <h3>Misi</h3>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-default btn-sm"><a href="{{url('/misi/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
  </div>
  <div class="panel-body">

  <table class="table table-default">
    <thead>
      <tr>
        <th>id</th>
        <th>Misi</th>
        <th>Nomor Urut Calon</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($misi as $index=>$item)
      <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->misi}}</td>
        <td>{{$item->kandidat_nomor}}</td>
        <td>
          <a href="{{url('misi/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
          <a href="{{url('misi/edit',[$item->id])}}" class="btn btn-primary btn-sm">edit</a>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
