@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/visi')}}">Visi</a></li>
  <li class="active">Form Visi</li>
</ol>
  <div class="page-header">
    <h3>Input Data Visi</h3>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Visi
        </div>
          <div class="panel-body">
            {!! Form::open() !!}

            <div class="form-group{{ $errors->has('visi') ? ' has-error' : '' }}">
              {!! Form::label('visi') !!}
              {!! Form::textarea('visi', old('visi'),['class'=>'form-control']) !!}
              @if ($errors->has('visi'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('visi') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('calon_id') ? ' has-error' : '' }}">
              {!! Form::label('calon ') !!}
              {!! Form::select('calon_id', $kandidat, old('calon_id'),['class'=>'form-control']) !!}
              @if ($errors->has('calon_id'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('calon_id') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>

@endsection
