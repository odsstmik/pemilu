  @extends('manager.layout.index')
  @section('content')
@if(Auth::user()->status=='0')
    <div class="page-header">
      <h3>Selamat  Datang</h3>
    </div>

    <div class="row">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-red">
              <div class="panel-heading">
                  <div class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-sitemap fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div class="huge">{{$panitia->count()}}</div>
                          <div>Panitia !</div>
                      </div>
                  </div>
              </div>
              <a href="{{url('/home/panitia')}}">
                  <div class="panel-footer">
                      <span class="pull-left">View Details</span>
                      <span class="pull-right"> <i class="fa fa-arrow-circle-right" ></i></span>
                      <div class="clearfix"></div>
                  </div>
              </a>
          </div>
      </div>
    </div>

    @elseif(Auth::user()->status=='1')

    <div class="row">
      <div class="col-sm-12 col-md-12 col-lg-12" style="text-align:center;">
        <h1 style="font-weight:bold;">SELAMAT DATANG</h1>
        <h2>{{Auth::user()->name}}</h2>
        <h3>Gunakan hak pilih anda di sini !!!</h3>
        <br>
        <a href="#" class="btn btn-primary">Lihat Kandidat</a>
        <a href="#" class="btn btn-success">Memilih</a>
      </div>
    </div>

    @else

    <div class="text-center">
      akun anda tidak dapat di ketahui
    </div>
    @endif
  @endsection
