@extends('manager.layout.index')
@section('content')

  <div class="page-header">
    <h2>User</h2>
  </div>
  <div class="row">
    <div class="col-sm-12 col-md-12 col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
            <button class="btn btn-default btn-sm"><a href="{{url('users/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
        </div>
        <div class="panel-body">
          <table class="table table-default">
            <thead>
              <tr>
                <th>Nama</th>
                <th>jabatan</th>
                <th>Role</th>
                <th width="200">Action</th>
              </tr>
            </thead>
            <tbody>
              @foreach($users as $index=>$item)
              <tr>
                <td>{{$item->name}}</td>
                <td>{{$item->email}}</td>
                <td>{{$item->status_label}}</td>
                <td>{{$item->role_label}}</td>
                <td>
                <a href="{{url('users/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
                  <!-- <a href="{{url('users/edit',[$item->id])}}" class="btn btn-primary btn-sm">edit</a> -->
                </td>

              </tr>
              @endforeach
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
@endsection
