@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/users')}}">User</a></li>
  <li class="active">Form User</li>
</ol>

  <div class="page-header">
    <h2>Form User</h2>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          User
        </div>
          <div class="panel-body">
            {!! Form::open() !!}
            <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
              {!! Form::label('nama') !!}
              {!! Form::text('name', old('name'),['class'=>'form-control']) !!}
              @if ($errors->has('name'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('name') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
              {!! Form::label('Email') !!}
              {!! Form::text('email', old('email'),['class'=>'form-control']) !!}
              @if ($errors->has('email'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
              {!! Form::label('Password') !!}
              {!! Form::password('password', old('password'),['class'=>'form-control']) !!}
              @if ($errors->has('password'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('status') ? ' has-error' : '' }}">
              {!! Form::label('Status') !!}
              {!! Form::select('status',['0' => 'Administrator', '1' => 'Mahasiswa'], old('status'),['class'=>'form-control']) !!}
              @if ($errors->has('status'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('status') }}</strong>
                  </span>
              @endif
            </div>
            <div class="form-group{{ $errors->has('role') ? ' has-error' : '' }}">
              {!! Form::label('Role') !!}
              {!! Form::select('role', ['0' => 'Active', '1' => 'Deactive'], old('role'),['class'=>'form-control']) !!}
              @if ($errors->has('role'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('role') }}</strong>
                  </span>
              @endif
            </div>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>
@endsection
