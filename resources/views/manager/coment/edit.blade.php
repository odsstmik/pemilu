@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/karya')}}">Komentar</a></li>
  <li class="active">Form Komentar</li>
</ol>
  <div class="page-header">
    <h3>Input Data Komentar</h3>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Komentar
        </div>
          <div class="panel-body">
            {!! Form::open() !!}
            {!! Form::model($coment) !!}


            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::label('nama') !!}
              {!! Form::text('nama', old('nama'),['class'=>'form-control']) !!}
              @if ($errors->has('nama'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('isi') ? ' has-error' : '' }}">
              {!! Form::label('isi') !!}
              {!! Form::textarea('isi', old('isi'),['class'=>'form-control']) !!}
              @if ($errors->has('isi'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('isi') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('calon_id') ? ' has-error' : '' }}">
              {!! Form::label('calon ') !!}
              {!! Form::select('calon_id', $kandidat, old('calon_id'),['class'=>'form-control']) !!}
              @if ($errors->has('calon_id'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('calon_id') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>

@endsection
