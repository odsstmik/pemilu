@extends('manager.layout.index')
@section('content')
<div class="page-header">
  <h3>Komentar</h3>
</div>
<div class="panel panel-default">
  <div class="panel-heading">
    <button class="btn btn-default btn-sm"><a href="{{url('/coment/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
  </div>
  <div class="panel-body">
  <table class="table table-default">
    <thead>
      <tr>
        <th>id</th>
        <th>Nama</th>
        <th>Nomor Urut Calon</th>
        <th>Komentar</th>
        <th>Action</th>
      </tr>
    </thead>
    <tbody>
      @foreach($coment as $index=>$item)
      <tr>

        <td>{{$item->id}}</td>
        <td>{{$item->nama}}</td>
        <td>{{$item->kandidat_nomor}}</td>
        <td>{{$item->isi}}</td>
        <td>
          <a href="{{url('coment/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
          <a href="{{url('coment/edit',[$item->id])}}" class="btn btn-primary btn-sm">edit</a>
        </td>

      </tr>
      @endforeach
    </tbody>
  </table>
</div>
</div>
@endsection
