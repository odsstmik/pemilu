@extends('manager.layout.index')
@section('content')
@if(Auth::user()->status == '0')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/karya')}}">Komentar</a></li>
  <li class="active">Form Komentar</li>
</ol>
  <div class="page-header">
    <h3>Input Data Komentar</h3>
  </div>
  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Komentar
        </div>
          <div class="panel-body">
            {!! Form::open() !!}

            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::label('nama') !!}
              {!! Form::text('nama', old('nama'),['class'=>'form-control']) !!}
              @if ($errors->has('nama'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('isi') ? ' has-error' : '' }}">
              {!! Form::label('isi') !!}
              {!! Form::textarea('isi', old('isi'),['class'=>'form-control']) !!}
              @if ($errors->has('isi'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('isi') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group">
              {!! Form::label('calon ') !!}
              {!! Form::select('calon_id', $kandidat, old('calon_id'),['class'=>'form-control','placeholder'=>'Pilih Kandidat...']) !!}
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>
  @elseif(Auth::user()->status == '1')
    <div class="row">
      <div class="col-lg-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            Komentar
          </div>
            <div class="panel-body">
              {!! Form::open() !!}

              <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
                {!! Form::label('nama') !!}
                {!! Form::text('nama', old('nama'),['class'=>'form-control']) !!}
                @if ($errors->has('nama'))
                    <span id="helpBlock2" class="help-block">
                        <strong>{{ $errors->first('nama') }}</strong>
                    </span>
                @endif
              </div>
              <br>
              <div class="form-group{{ $errors->has('isi') ? ' has-error' : '' }}">
                {!! Form::label('isi') !!}
                {!! Form::textarea('isi', old('isi'),['class'=>'form-control']) !!}
                @if ($errors->has('isi'))
                    <span id="helpBlock2" class="help-block">
                        <strong>{{ $errors->first('isi') }}</strong>
                    </span>
                @endif
              </div>
              <br>
              <div class="form-group">
                {!! Form::label('calon ') !!}
                {!! Form::select('calon_id', $kandidat, old('calon_id'),['class'=>'form-control','placeholder'=>'Pilih Kandidat...']) !!}
              </div>
              <br>
              <button type="submit" class="btn btn-primary btn-sm">Submit</button>
              {!! Form::close() !!}
            </div>
        </div>
      </div>
    </div>
    <div class="col-sm-10 col-md-10 col-lg-10">
      <div class="table-responsive">
        <table class="table table-hover">
            <tbody>
              @foreach($coment as $index=>$item)
              <tr>
                <td style="padding:15px;"><p><b>{{$item->nama}}</b> <small>{{$item->isi}}</small></p></td>
              </tr>
              @endforeach
            </tbody>
        </table>
      </div>
    </div>
    </div>
    @else

    <h1>halaman tidak di temukan</h1>


    @endif
@endsection
