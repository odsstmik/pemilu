@extends('manager.layout.index')
@section('content')
@if(Auth::user()->status=='0')
  <div class="page-header">
    <h3>Panitia </h3>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      <button class="btn btn-default btn-sm"><a href="{{url('/panitia/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
    </div>
    <div class="panel-body">

        <table class="table table-default">
          <thead>
            <tr>
              <th>Nama</th>
              <th>jabatan</th>
              <th width="200">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($panitia as $index=>$item)
            <tr>
              <td>{{$item->nama}}</td>
              <td>{{$item->jabatan}}</td>
              <td>
                <a href="{{url('panitia/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
                <a href="{{url('panitia/edit',[$item->id])}}" class="btn btn-primary btn-sm">edit</a>
              </td>

            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
@else(Auth::user()->status=='1')
  <div class="page-header">
    <h3>Panitia </h3>
  </div>
  <div class="panel panel-default">
    <div class="panel-heading">
      
    </div>
    <div class="panel-body">

        <table class="table table-default">
          <thead>
            <tr>
              <th>Nama</th>
              <th>jabatan</th>
              <th width="200">Action</th>
            </tr>
          </thead>
          <tbody>
            @foreach($panitia as $index=>$item)
            <tr>
              <td>{{$item->nama}}</td>
              <td>{{$item->jabatan}}</td>
              <td>
                <a href="{{url('panitia/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
                <a href="{{url('panitia/edit',[$item->id])}}" class="btn btn-primary btn-sm">edit</a>
              </td>

            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
  </div>
  @endif
@endsection
