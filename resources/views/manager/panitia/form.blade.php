@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/panitia')}}">Panitia</a></li>
  <li class="active">Form Panitia</li>
</ol>
  <div class="page-header">

  <h3>Input Data Panitia</h3>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Panitia
        </div>
          <div class="panel-body">
            {!! Form::open() !!}
            <div class="form-group{{ $errors->has('nama') ? ' has-error' : '' }}">
              {!! Form::label('nama') !!}
              {!! Form::text('nama', old('nama'),['class'=>'form-control']) !!}
              @if ($errors->has('nama'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('nama') }}</strong>
                  </span>
              @endif
            </div>

            <br>
            <div class="form-group{{ $errors->has('jabatan') ? 'has-error' : ''}}">
              {!! Form::label('jabatan') !!}
              {!! Form::text('jabatan', old('jabatan'),['class'=>'form-control']) !!}
              @if ($errors->has('jabatan'))
                <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('jabatan') }}</strong>
                  </span>
              @endif
            </div>

            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>

@endsection
