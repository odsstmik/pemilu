@extends('manager.layout.index')
@section('content')
<ol class="breadcrumb">
  <li><a href="{{url('/home')}}">Home</a></li>
  <li><a href="{{url('/home/kandidat')}}">Kandidat</a></li>
  <li class="active">Form Kandidat</li>
</ol>
  <div class="page-header">
    <h3>Input Data Kandidat</h3>
  </div>

  <div class="row">
    <div class="col-lg-12">
      <div class="panel panel-default">
        <div class="panel-heading">
          Kandidat
        </div>
          <div class="panel-body">
            {!! Form::open(['files'=>true]) !!}
            {!! Form::model($kandidat) !!}

            <div class="form-group{{ $errors->has('image') ? ' has-error' : '' }}">
              {!! Form::label('Calon Ketua') !!}
              {!! Form::file('image',null) !!}
              @if ($errors->has('image'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('image') }}</strong>
                  </span>
              @endif
            </div>

            <div class="form-group{{ $errors->has('ketua') ? ' has-error' : '' }}">
              {!! Form::label('Calon Ketua') !!}
              {!! Form::text('ketua', old('ketua'),['class'=>'form-control']) !!}
              @if ($errors->has('ketua'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('ketua') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('wakil') ? ' has-error' : '' }}">
              {!! Form::label('Calon Wakil') !!}
              {!! Form::text('wakil', old('wakil'),['class'=>'form-control']) !!}
              @if ($errors->has('wakil'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('wakil') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <div class="form-group{{ $errors->has('nomor') ? ' has-error' : '' }}">
              {!! Form::label('Nomor Urut') !!}
              {!! Form::text('nomor', old('nomor'),['class'=>'form-control']) !!}
              @if ($errors->has('nomor'))
                  <span id="helpBlock2" class="help-block">
                      <strong>{{ $errors->first('nomor') }}</strong>
                  </span>
              @endif
            </div>
            <br>
            <button type="submit" class="btn btn-primary btn-sm">Submit</button>
            {!! Form::close() !!}
          </div>
      </div>
    </div>
  </div>

@endsection
