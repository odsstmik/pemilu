@extends('manager.layout.index')
@section('content')
<div class="page-header">
  <h3>Kandidat</h3>
    <button class="btn btn-default btn-sm"><a href="{{url('/kandidat/create')}}" style="color:black;text-decoration:none;"><i class="fa fa-plus"></i> Tambah</a></button>
</div>

<div class="row">
  @foreach($kandidat as $index=>$item)
  <div class="col-sm-4 col-md-4 col-lg-4">
    <div class="panel panel-primary">
      <div class="panel-heading">
          <a href="{{url('kandidat/edit',[$item->id])}}" style="color:white;"><button class="btn btn-sm btn-primary"><i class="fa fa-pencil"></i></button></a>
          <a href="{{url('kandidat/delete',['$item->id'])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" style="color:white;"><button class="btn btn-sm btn-primary"><i class="fa fa-times"></i></button></a>
      </div>
      <div class="panel-body">
            <img src="{{$item->image}}" class="img-responsive center-block" >
      </div>
      <div class="panel-footer">
        <p>Pasangan Nomer Urut : {{$item->nomor}}</p>
        <p>Calon Ketua : {{$item->ketua}} </p>
        <a href="{{url('kandidat/detail', ['$item->id'])}}">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"> <i class="fa fa-arrow-circle-right" ></i></span>
                <div class="clearfix"></div>
            </div>
        </a>
        <!-- <a href="#">
            <div class="panel-footer">
              <span class="pull-left"><a href="{{url('kandidat/detail',[$item->id])}}">Liat Selengkapnya</a></span>
                <span class="pull-right"> <i class="fa fa-arrow-circle-right" ></i></span>
                <div class="clearfix"></div>
                <a href="{{url('kandidat/edit',[$item->id])}}" class="btn btn-primary btn-sm">Edit</a>
                <span class="pull-left"></span>
                <a href="{{url('kandidat/delete',[$item->id])}}" onclick="return confirm('Anda yakin akan menghapus data ini ?');" class="btn btn-danger btn-sm">Hapus</a>
                <span class="pull-left"></span>
            </div>
        </a> -->
      </div>
    </div>
  </div>
  @endforeach
</div>

@endsection
