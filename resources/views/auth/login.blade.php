<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <title>Login Site</title>

  <link rel="stylesheet" href="{{url('https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css')}}">

  <link rel="stylesheet prefetch" href="{{url('http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900')}}">
<link rel="stylesheet prefetch" href="{{ url('http://fonts.googleapis.com/css?family=Montserrat:400,700')}}">
<link rel="stylesheet prefetch" href="{{ url('https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css')}}">

      <link rel="stylesheet" href="{{ url('template/login/css/style.css')}}">

</head>
<body>
<br>
<br>
<br>
<br>
<div class="form">
  <div class="header_"><img src="{{ url('template/login/img/login-logo.png')}}" width="500px;"></div>
  <br>
  <br>
  <br>
  <form class="login-form" role="form" method="POST" action="{{ url('/login') }}">
      {{ csrf_field() }}

      <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
          <div class="col-md-6">
              <input id="email" type="email" class="form-control" name="email" placeholder="username" value="{{ old('email') }}"  required >

              @if ($errors->has('email'))
                  <span class="help-block">
                      <strong>{{ $errors->first('email') }}</strong>
                  </span> 
              @endif
          </div>
      </div>

      <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
          <div class="col-md-6">
              <input id="password" type="password" class="form-control" name="password" placeholder="password" required>

              @if ($errors->has('password'))
                  <span class="help-block">
                      <strong>{{ $errors->first('password') }}</strong>
                  </span>
              @endif
          </div>
      </div>
      <div class="form-group">
          <div class="col-md-8 col-md-offset-4">
              <button type="submit" class="btn btn-primary">
                  Login
              </button>
          </div>
      </div>
  </form>
</div>
</body>
</html>
