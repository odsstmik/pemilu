<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\KaryaIlmiah;
use App\Kandidat;
class KaryaIlmiahController extends Controller
{
     public function __construct(KaryaIlmiah $karya_ilmiah, Kandidat $kandidat, Request $request){
      $this->karya_ilmiah = $karya_ilmiah;
      $this->kandidat = $kandidat;
       $this->request = $request;
     }
     public function Index()
     {
       $karya_ilmiah = KaryaIlmiah::All();
       $kandidat = Kandidat::pluck('nomor', 'id');
       //return $panitia;
      return view('manager.karya_ilmiah.index', compact('karya_ilmiah', 'kandidat'));
    }
    public function create()
    {
      $kandidat = Kandidat::pluck('nomor', 'id');
      return view('manager.karya_ilmiah.form', compact('kandidat'));
    }
    public function submit(Request $request)
    {
      $karya_ilmiah = new KaryaIlmiah;
      $this->validate($request,[
        'judul' => 'required',
        'isi' => 'required',
        'calon_id' => 'required',
      ]);
      $karya_ilmiah->judul = $this->request['judul'];
      $karya_ilmiah->isi = $this->request['isi'];
      $karya_ilmiah->calon_id = $this->request['calon_id'];
      $karya_ilmiah->save();
      return redirect('home/karya');
    }
    public function delete($id)
    {
         $karya_ilmiah = $this->karya_ilmiah->findOrFail($id);
         $karya_ilmiah->delete();
          return redirect('home/karya');
    }
    public function edit($id)
    {
      $karya_ilmiah = KaryaIlmiah::find($id);
      $kandidat = Kandidat::pluck('nomor', 'id');
      return view('manager.karya_ilmiah.edit',compact('karya_ilmiah' ,'kandidat'));
    }
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'judul' => 'required',
        'isi' => 'required',
        'calon_id' => 'required',
      ]);
      $karya_ilmiah = KaryaIlmiah::find($id);
      $karya_ilmiahUpdate = $request->all();
      $karya_ilmiah->update($karya_ilmiahUpdate);
      return redirect('home/karya');
    }
}
