<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Coment;
use App\Kandidat;
use App\User;
class ComentController extends Controller
{
  public function __construct(Coment $coment, Kandidat $kandidat, User $user, Request $request)
  {
    $this->coment = $coment;
    $this->kandidat = $kandidat;
    $this->user = $user;
    $this->request = $request;
  }

  public function index()
  {
    $coment = Coment::All();
    $user = $this->user->paginate(10);
    $kandidat = Kandidat::pluck('nomor', 'id');

    return view('manager.coment.index', compact('coment', 'kandidat','user'));
  }

  public function create()
  {
      $coment = coment::all();
      $kandidat = Kandidat::pluck('nomor', 'id');
      $user = user::all();
      return view('manager.coment.form', compact('coment','kandidat','user'));

  }

  public function submit(Request $request)
  {
    $coment = new Coment;
    $this->validate($request,[
      'nama' => 'required',
      'isi' => 'required',
    ]);

    $coment->nama = $this->request['nama'];
    $coment->calon_id = $this->request['calon_id'];
    $coment->isi = $this->request['isi'];

    $coment->save();
    if(app('auth')->user()->status=='0'){
      return redirect('home/coment');
    }
    elseif(app('auth')->user()->status=='1'){
      return redirect('coment/create');
    }
    else {
      return redirect('home');
    }
  }

  public function delete($id)
  {
    $coment = $this->coment->findOrFail($id);
    $coment->delete();

    return redirect('home/coment');
  }

  public function edit($id)
  {
    $coment = Coment::find($id);
    $kandidat = Kandidat::pluck('nomor', 'id');
    return view('manager.coment.edit',compact('coment','kandidat'));
  }
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'nama' => 'required',
      'calon_id' => 'required',
      'isi' => 'required',
    ]);
    $coment = Coment::find($id);
    $comentUpdate = $request->all();
    $coment->update($comentUpdate);
    return redirect('home/coment');
  }
}
