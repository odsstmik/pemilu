<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kandidat;
class WebController extends Controller
{
  public function __construct(Kandidat $kandidat, Request $request){
      $this->kandidat = $kandidat;
      $this->request = $request;
  }
    public function index()
    {
      $kandidat = Kandidat::all();
      return view('web.index',compact('kandidat'));
    }
}
