<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Panitia;


class PanitiaController extends Controller
{
  public function __construct(Panitia $panitia, Request $request){
      $this->panitia = $panitia;
       $this->request = $request;
  }

  public function Index()
  {   $panitia = Panitia::All();

    //return $panitia;
    return view('manager.panitia.index', compact('panitia'));
  }
  public function create()
  {
    return view('manager.panitia.form');
  }
  public function submit(Request $request)
  {
    $panitia = new Panitia;
    $this->validate($request,[
      'nama' => 'required',
      'jabatan' => 'required',
    ]);
    $panitia->nama = $this->request['nama'];
    $panitia->jabatan = $this->request['jabatan'];
    $panitia->save();
    return redirect('home/panitia');
  }
  public function edit($id)
     {
       $panitia = Panitia::find($id);
       return view('manager.panitia.edit',compact('panitia'));
     }
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'nama' => 'required',
      'jabatan' => 'required',
    ]);

    $panitia = Panitia::find($id);
    $panitiaUpdate = $request->all();
    $panitia->update($panitiaUpdate);
    return redirect('home/panitia');
  }

  public function delete($id)
  {
       $panitia = $this->panitia->findOrFail($id);
       $panitia->delete();
        return redirect('home/panitia');
  }
}
