<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kandidat;

class WebkandidatController extends Controller
{
    public function index()
    {
      $kandidat = Kandidat::All();
      return view ('web.kandidat', compact('kandidat'));
    }
}
