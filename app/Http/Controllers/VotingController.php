<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Voting;
use App\kandidat;
class VotingController extends Controller
{
    public function __construct(Voting $voting, Kandidat $kandidat, Request $request)
    {
      $this->voting = $voting;
      $this->kandidat = $kandidat;
      $this->request = $request;
    }

    public function index()
    {
      $voting = Voting::all();

      return view('manager.voting.index', compact('voting'));
    }

    public function create()
    {
      $kandidat = Kandidat::pluck('nomor', 'id');

      return view('manager.voting.form', compact('kandidat'));
    }

    public function submit(Request $request)
    {
      $voting = new Voting;
      $this->validate($request, [
        'calon_id' => 'required'
      ]);

      $voting->calon_id = $this->request['calon_id'];
      $voting->jumlah = 0;
      $voting->save();

      return redirect('home\voting');
    }

    public function delete($id)
    {
         $voting = $this->voting->findOrFail($id);
         $voting->delete();
          return redirect('home/voting');
    }

    public function edit($id)
    {
      $voting = Voting::find($id);
      $kandidat = Kandidat::pluck('nomor', 'id');
      return view('manager.voting.edit',compact('voting' ,'kandidat'));
    }

    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'calon_id' => 'required',
      ]);
      $voting = Voting::find($id);
      $votingUpdate = $request->all();
      $voting->update($votingUpdate);
      return redirect('home/voting');
    }
}
