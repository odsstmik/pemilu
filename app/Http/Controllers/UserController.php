<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
class UserController extends Controller
{
    public function __construct(User $users, Request $request){
        $this->users = $users;
         $this->request = $request;
    }

    public function index(){
        $users = User::all();
        //return $users;
        return view('manager.user.index',compact('users'));
    }
    public function create()
    {
      return view('manager.user.form');
    }
    public function submit(Request $request)
    {
        $users = new User;
        $this->validate($request,[
          'name' => 'required',
          'email' => 'required',
          'password' => 'required',
          'status' => 'required',
          'role' => 'required',
        ]);

        $users->name = $this->request['name'];
        $users->email = $this->request['email']; //ubah biar bisa di input email
        $users->password = bcrypt($this->request['password']);//ubah biar bisa nambah password
        $users->status = $this->request['status'];// + tambahkan status sama role di db:user
        $users->role = $this->request['role'];// tq semangat wkwk
        //$users->status = 0;
        $users->save();
        return $this->index();
      }

      public function delete($id){
          $user = $this->users->findOrFail($id);
          $user->delete();
          return redirect('home/users');
      }
}
