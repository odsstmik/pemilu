<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Panitia;

class WebpanitiaController extends Controller
{
  public function index()
  {
    $panitia = Panitia::all();
    return view('web.panitia',compact('panitia'));
  }
}
