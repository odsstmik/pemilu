<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Misi;
use App\Kandidat;
class MisiController extends Controller
{
  public function __construct(Misi $misi, Kandidat $kandidat, Request $request)
  {
    $this->misi = $misi;
    $this->kandidat = $kandidat;
    $this->request = $request;
  }

  public function index()
  {
    $misi = Misi::All();
    $kandidat = Kandidat::pluck('nomor', 'id');

    return view('manager.misi.index', compact('misi', 'kandidat'));
  }

  public function create()
  {
    $kandidat = Kandidat::pluck('nomor', 'id');
    return view('manager.misi.form', compact('kandidat'));
  }

  public function submit(Request $request)
  {
    $misi = new Misi;
    $this->validate($request,[
      'misi' => 'required',
      'calon_id' => 'required',
    ]);

    $misi->misi = $this->request['misi'];
    $misi->calon_id = $this->request['calon_id'];

    $misi->save();

    return redirect('home/misi');
  }

  public function delete($id)
  {
    $misi = $this->misi->findOrFail($id);
    $misi->delete();

    return redirect('home/misi');
  }

  public function edit($id)
  {
    $misi = misi::find($id);
    $kandidat = kandidat::pluck('nomor', 'id');
    return view('manager.misi.edit',compact('misi', 'kandidat'));
  }
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'misi' => 'required',
      'calon_id' => 'required',
    ]);
    $misi = misi::find($id);
    $misiUpdate = $request->all();
    $misi->update($misiUpdate);
    return redirect('home/misi');
  }
}
