<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Visi;
use App\kandidat;
class VisiController extends Controller
{
  public function __construct(Visi $visi, Kandidat $kandidat, Request $request)
  {
    $this->visi = $visi;
    $this->kandidat = $kandidat;
    $this->request = $request;
  }

  public function index()
  {
    $visi = Visi::All();
    $kandidat = Kandidat::pluck('nomor', 'id');

    return view('manager.visi.index', compact('visi', 'kandidat'));
  }

  public function create()
  {
    $kandidat = Kandidat::pluck('nomor', 'id');
    return view('manager.visi.form', compact('kandidat'));
  }

  public function submit(Request $request)
  {
    $visi = new Visi;
    $this->validate($request,[
      'visi' => 'required',
      'calon_id' => 'required',
    ]);

    $visi->visi = $this->request['visi'];
    $visi->calon_id = $this->request['calon_id'];

    $visi->save();

    return redirect('home/visi');
  }

  public function delete($id)
  {
    $visi = $this->visi->findOrFail($id);
    $visi->delete();

    return redirect('home/visi');
  }

  public function edit($id)
  {
    $visi = visi::find($id);
    $kandidat = kandidat::pluck('nomor', 'id');
    return view('manager.visi.edit',compact('visi', 'kandidat'));
  }
  public function update(Request $request, $id)
  {
    $this->validate($request,[
      'visi' => 'required',
      'calon_id' => 'required',
    ]);
    $visi = visi::find($id);
    $visiUpdate = $request->all();
    $visi->update($visiUpdate);
    return redirect('home/visi');
  }
}
