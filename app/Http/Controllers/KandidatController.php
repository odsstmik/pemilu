<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\kandidat;
class KandidatController extends Controller
{
    public function __construct(Kandidat $kandidat, Request $request)
    {
      $this->kandidat = $kandidat;
      $this->request = $request;
    }

    public function index()
    {
      $kandidat = kandidat::All();
     
      return view('manager.kandidat.index', compact('kandidat'));
    }

    public function create()
    {
      return view('manager.kandidat.form');
    }

    public function submit(Request $request)
    {
      $kandidat = new Kandidat;
      $this->validate($request,[
        'ketua' => 'required',
        'wakil' => 'required',
        'nomor' => 'required',
        'image' => 'required',
      ]);

      $kandidat->ketua = $this->request['ketua'];
      $kandidat->wakil = $this->request['wakil'];
      $kandidat->nomor = $this->request['nomor'];

      $file = $this->request->file('image');
      $path = public_path('image');
      $fileName = uniqid(). $file->getClientOriginalName();
      $url = url('image', $fileName);
      /* pindahin file yang ke uploadnya ke folder nya */
      $file->move($path, $fileName);
      /* simpan alamat filenya ke field database */
      $kandidat->image = $url; // ini urlnya full pake http://dst/namafile.jpg
      //$slider->image = $fileName;  // ini cuman nama filenya jadi pas di view mesti pake {{ url('image', $item->image) }}

      $kandidat->save();

      return redirect('home/kandidat');
    }

    public function delete($id)
    {
      $kandidat = $this->kandidat->findOrFail($id);
      $kandidat->delete();
      $filename = basename($kandidat->image);
      $path = public_path('image/' . $filename);
      @unlink($path);

      return redirect('home/kandidat');
    }

    public function edit($id)
    {
      $kandidat = Kandidat::find($id);
      return view('manager.kandidat.edit',compact('kandidat'));
    }
    public function update(Request $request, $id)
    {
      $this->validate($request,[
        'ketua' => 'required',
        'wakil' => 'required',
        'nomor' => 'required',
        'image' => 'required',
      ]);
      $kandidat = Kandidat::find($id);
      $kandidatUpdate = $request->all();
      $kandidat->update($kandidatUpdate);
      return redirect('home/kandidat');
    }

    public function detail($id)
    {
      $kandidat = $this->kandidat->where('id', $id)->get();

      return view('manager.kandidat.detail', compact('kandidat'));
    }
}
