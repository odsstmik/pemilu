<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Misi extends Model
{
  protected $table = 'misi';
  protected $fillable = ['misi', 'calon_id'];
  protected $appends = ['kandidat_nomor'];

  public function Kandidat()
  {
    return $this->belongsTo('App\Kandidat', 'calon_id');
  }

  public function getKandidatNomorAttribute()
  {
    return object_get($this->kandidat, 'nomor', '-');
  }
}
