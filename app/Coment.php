<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Coment extends Model
{
    protected $table = 'coment';
    protected $fillable = ['nama', 'calon_id', 'isi'];
    protected $appends = ['kandidat_nomor'];

    public function Kandidat()
    {
      return $this->belongsTo('App\Kandidat', 'calon_id');
    }

    public function getKandidatNomorAttribute()
    {
      return object_get($this->kandidat, 'nomor', '-');
    }
}
