<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = ['status_label', 'role_label'];

    public function getStatusLabelAttribute()
    {
      $template = '%s';

      switch ($this->status) {
        case '1':
          $status = 'Mahasiswa';
          break;

        default:
          $status = 'Administrator';
          break;
      }

      return sprintf($template, $status);
    }

    public function getRoleLabelAttribute()
    {
      $template = '%s';

      switch ($this->role) {
        case '1':
          $role = 'Deactive';
          break;

        default:
          $role = 'Active';
          break;
      }

      return sprintf($template, $role);
    }
}
