<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KaryaIlmiah extends Model
{
    protected $table= 'karya_ilmiah';
    protected $fillable= ['judul','isi', 'calon_id'];
    protected $appends = ['kandidat_nomor'];

    public function Kandidat()
    {
      return $this->belongsTo('App\Kandidat', 'calon_id');
    }

    public function getKandidatNomorAttribute()
    {
      return object_get($this->kandidat, 'nomor', '-');
    }
}
