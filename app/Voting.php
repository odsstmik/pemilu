<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voting extends Model
{
    protected $table = 'voting';
    protected $fillable = ['calon_id', 'jumlah'];
    protected $appends = ['display_calon'];

    public function Kandidat()
    {
      return $this->belongsTo('App\Kandidat', 'calon_id');
    }

    public function getDisplayCalonAttribute()
    {
        $kandidat = $this->kandidat;
        if($kandidat){
            $format = ' %s -  %s';
            return sprintf($format, $kandidat->ketua, $kandidat->wakil);
        }else{
            return 'Tidak ada kandidat';
        }
    }

    public function getKandidatNomorAttribute()
    {
      return object_get($this->kandidat, 'nomor', '-');
    }
}
