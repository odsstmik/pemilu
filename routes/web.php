<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function(){
  return redirect('web');
});
Route::get('/panitia','WebpanitiaController@index');
Route::get('/kandidat','WebkandidatController@index');
Route::get('/about','WebaboutController@index');
Route::group(['prefix'=>'web'], function(){
  Route::get('/','WebController@index');
});
Auth::routes();

//Route::get('/home', 'HomeController@index');
Route::group(['middleware' => 'auth'], function () {
  Route::get('/home', 'HomeController@index');
  Route::get('/home/users','UserController@index');
  Route::get('/home/panitia','PanitiaController@index');
  Route::get('/home/karya', 'KaryaIlmiahController@index');
  Route::get('/home/kandidat', 'KandidatController@index');
  Route::get('/home/coment', 'ComentController@index');
  Route::get('/home/visi', 'VisiController@index');
  Route::get('/home/misi', 'MisiController@index');
  Route::get('/home/voting', 'VotingController@index');
  Route::get('/home/vote', 'VoteController@getIndex');

  Route::group(['prefix'=>'users'], function(){
    Route::get('home/users','UserController@index');
    Route::get('create','UserController@create');
    Route::post('create','UserController@submit');
    Route::get('edit/{id}','UserController@edit');
    Route::post('edit/{id}','UserController@update');
    Route::get('delete/{id}','UserController@delete');

  });

  Route::group(['prefix'=>'panitia'], function(){
    Route::get('home/panitia','PanitiaController@index');
    Route::get('create','PanitiaController@create');
    Route::post('create','PanitiaController@submit');
    Route::get('edit/{id}','PanitiaController@edit');
    Route::post('edit/{id}','PanitiaController@update');
    Route::get('delete/{id}','PanitiaController@delete');

  });
Route::group(['prefix'=>'karya'], function(){
    Route::get('home/karya','KaryaIlmiahController@index');
    Route::get('create','KaryaIlmiahController@create');
    Route::post('create','KaryaIlmiahController@submit');
    Route::get('edit/{id}','KaryaIlmiahController@edit');
    Route::post('edit/{id}','KaryaIlmiahController@update');
    Route::get('delete/{id}','KaryaIlmiahController@delete');

});
  Route::group(['prefix'=>'kandidat'], function(){
      Route::get('home/kandidat','KandidatController@index');
      Route::get('create','KandidatController@create');
      Route::post('create','KandidatController@submit');
      Route::get('edit/{id}','KandidatController@edit');
      Route::post('edit/{id}','KandidatController@update');
      Route::get('delete/{id}','KandidatController@delete');
      Route::get('detail/{id}','KandidatController@detail');
  });

  Route::group(['prefix'=>'coment'], function(){
      Route::get('home/coment','ComentController@index');
      Route::get('create','ComentController@create');
      Route::post('create','ComentController@submit');
      Route::get('edit/{id}','ComentController@edit');
      Route::post('edit/{id}','ComentController@update');
      Route::get('delete/{id}','ComentController@delete');
  });

  Route::group(['prefix'=>'visi'], function(){
      Route::get('home/visi','VisiController@index');
      Route::get('create','VisiController@create');
      Route::post('create','VisiController@submit');
      Route::get('edit/{id}','VisiController@edit');
      Route::post('edit/{id}','VisiController@update');
      Route::get('delete/{id}','VisiController@delete');
  });

  Route::group(['prefix'=>'misi'], function(){
      Route::get('home/misi','MisiController@index');
      Route::get('create','MisiController@create');
      Route::post('create','MisiController@submit');
      Route::get('edit/{id}','MisiController@edit');
      Route::post('edit/{id}','MisiController@update');
      Route::get('delete/{id}','MisiController@delete');
  });

  Route::group(['prefix'=>'voting'], function(){
      Route::get('home/voting','VotingController@index');
      Route::get('create','VotingController@create');
      Route::post('create','VotingController@submit');
      Route::get('edit/{id}','VotingController@edit');
      Route::post('edit/{id}','VotingController@update');
      Route::get('delete/{id}','VotingController@delete');
  });

  Route::group(['prefix'=>'vote'], function(){
    Route::get('home/vote','VoteController@getindex');
      Route::get('read','VoteController@getRead');
      Route::post('create','VoteController@postRead');
      Route::post('create','VoteController@save');
      Route::get('delete/{id}','VoteController@delete');
  });


    Route::group(['prefix'=>'users'], function(){
      Route::get('/home/users','UserController@index');
      Route::get('create','UserController@create');
      Route::post('create','UserController@submit');
      Route::get('delete/{id}','UserController@delete');
    });
      //Route::get('remove','PanitiaController@remove');
  });
